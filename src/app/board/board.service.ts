import { Array } from 'core-js/library/web/timers';
import { Tile } from './tile';
import { Injectable } from '@angular/core';


@Injectable()
export class BoardService {

  private board: Tile[][];
  private numberOfMines: number;

  constructor() {
  }

  generateBoard(rows: number, cols: number, numberOfMines: number): Tile[][] {
    this.numberOfMines = numberOfMines;
    this.board = [];

    for (let i = 0; i < rows; i++) {
      this.board[i] = [];
      for (let j = 0; j < cols; j++) {
        this.board[i][j] = { numberOfMines: 0, isAMine: false, column: j, row: i, hasFlag: false, touched: false };
      }
    }

    this.calculateMinesPositions(rows, cols, numberOfMines);

    return this.board;
  }

  calculateMinesPositions(rows: number, cols: number, numberOfMines: number): void {
    for (let i = 0; i < numberOfMines; i++) {
      this.setMinePosition(rows, cols);
    }
  }

  private searchAdjacentMines(tile: Tile) {
    const topRow = tile.row - 1;
    const bottomRow = tile.row + 1;

    const backColumn = tile.column - 1;
    const nextColumn = tile.column + 1;

    const searchInBackColumn = backColumn >= 0;

    const searchInNextColumn = nextColumn < this.board[0].length;

    if (topRow >= 0) {
      if (searchInBackColumn && this.board[topRow][backColumn].isAMine) {
        tile.numberOfMines++;
      }

      if (this.board[topRow][tile.column].isAMine) {
        tile.numberOfMines++;
      }

      if (searchInNextColumn && this.board[topRow][nextColumn].isAMine) {
        tile.numberOfMines++;
      }
    }

    if (searchInBackColumn && this.board[tile.row][backColumn].isAMine) {
      tile.numberOfMines++;
    }

    if (searchInNextColumn && this.board[tile.row][nextColumn].isAMine) {
      tile.numberOfMines++;
    }

    if (bottomRow < this.board.length) {
      if (searchInBackColumn && this.board[bottomRow][backColumn].isAMine) {
        tile.numberOfMines++;
      }

      if (this.board[bottomRow][tile.column].isAMine) {
        tile.numberOfMines++;
      }

      if (searchInNextColumn && this.board[bottomRow][nextColumn].isAMine) {
        tile.numberOfMines++;
      }
    }
  }

  searchForMines(tile: Tile) {
    this.searchAdjacentMines(tile);

    if (tile.numberOfMines > 0) { return; }

    const topRow = tile.row - 1;
    const bottomRow = tile.row + 1;

    const backColumn = tile.column - 1;
    const nextColumn = tile.column + 1;

    const searchInBackColumn = backColumn >= 0;

    const searchInNextColumn = nextColumn < this.board[0].length;

    if (topRow >= 0) {
      if (searchInBackColumn && !this.board[topRow][backColumn].touched && !this.board[topRow][backColumn].hasFlag) {
        this.board[topRow][backColumn].touched = true;
        this.searchForMines(this.board[topRow][backColumn]);
      }

      if (!this.board[topRow][tile.column].touched && !this.board[topRow][tile.column].hasFlag) {
        this.board[topRow][tile.column].touched = true;
        this.searchForMines(this.board[topRow][tile.column]);
      }

      if (searchInNextColumn && !this.board[topRow][nextColumn].touched && !this.board[topRow][nextColumn].hasFlag) {
        this.board[topRow][nextColumn].touched = true;
        this.searchForMines(this.board[topRow][nextColumn]);
      }
    }

    if (searchInBackColumn && !this.board[tile.row][backColumn].touched && !this.board[tile.row][backColumn].hasFlag) {
      this.board[tile.row][backColumn].touched = true;
      this.searchForMines(this.board[tile.row][backColumn]);
    }

    if (searchInNextColumn && !this.board[tile.row][nextColumn].touched && !this.board[tile.row][nextColumn].hasFlag) {
      this.board[tile.row][nextColumn].touched = true;
      this.searchForMines(this.board[tile.row][nextColumn]);
    }

    if (bottomRow < this.board.length) {
      if (searchInBackColumn && !this.board[bottomRow][backColumn].touched && !this.board[bottomRow][backColumn].hasFlag) {
        this.board[bottomRow][backColumn].touched = true;
        this.searchForMines(this.board[bottomRow][backColumn]);
      }

      if (!this.board[bottomRow][tile.column].touched && !this.board[bottomRow][tile.column].hasFlag) {
        this.board[bottomRow][tile.column].touched = true;
        this.searchForMines(this.board[bottomRow][tile.column]);
      }

      if (searchInNextColumn && !this.board[bottomRow][nextColumn].touched && !this.board[bottomRow][nextColumn].hasFlag) {
        this.board[bottomRow][nextColumn].touched = true;
        this.searchForMines(this.board[bottomRow][nextColumn]);
      }
    }
  }

  getRemainingTiles(): number {
    const totalTiles = this.board.length * this.board[0].length;

    const tilesToTouch = totalTiles - this.numberOfMines;

    let remainingTiles = 0;

    this.board.forEach(row => {
      remainingTiles += row.filter(tile => !tile.touched && !tile.isAMine).length;
    });

    return remainingTiles;
  }

  getNumberOfMines(): number {
    return this.numberOfMines;
  }

  getNumberOfFlags(): number {
    let numberOfFlags = 0;
    this.board.forEach(row => {
      numberOfFlags += row.filter(tile => tile.hasFlag).length;
    });

    return numberOfFlags;
  }

  setMinePosition(rows: number, cols: number) {
    const rowPosition = Math.floor(Math.random() * rows);
    const colPosition = Math.floor(Math.random() * cols);

    if (!this.board[rowPosition][colPosition].isAMine) {
      this.board[rowPosition][colPosition].isAMine = true;
    } else {
      this.setMinePosition(rows, cols);
    }
  }

}
