export class Tile {
  public row: number;
  public column: number;
  public isAMine: boolean;
  public hasFlag: boolean;
  public numberOfMines: number;
  public touched: boolean;

}
