import { Location } from '@angular/common';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Tile } from './tile';
import { BoardService } from './board.service';
import { SetupService } from '../setup/setup.service';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Difficulty } from '../setup/difficulty';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, AfterViewInit {

  @Output() gameLosed = new EventEmitter();
  @Output() gameWon = new EventEmitter();

  @Output() updateRemainingMines: EventEmitter<any> = new EventEmitter();

  private selectedDifficulty: Difficulty;
  private gameFinished = false;

  public board: Tile[][];


  constructor(private boardService: BoardService, private setupService: SetupService) { }

  ngOnInit() {
    this.selectedDifficulty = this.setupService.getSelectedDifficulty();
    this.board = this.boardService.generateBoard(this.selectedDifficulty.rows,
      this.selectedDifficulty.columns, this.selectedDifficulty.mines);
  }

  ngAfterViewInit() {
    setTimeout(f => { this.updateRemainingMines.emit(this.setupService.getSelectedDifficulty().mines); }, 150);
  }

  getImageResource(tile: Tile) {
    if (tile.isAMine && tile.touched) {
      return this.selectedDifficulty.columns > 16 ? 'assets/mine_icon_sm.png' : 'assets/mine_icon.png';
    } else if (tile.hasFlag) {
      return this.selectedDifficulty.columns > 16 ? 'assets/flag_icon_sm.png' : 'assets/flag_icon.png';
    } else {
      return this.selectedDifficulty.columns > 16 ? 'assets/tile_icon_sm.png' : 'assets/tile_icon.png';
    }
  }

  tileOnRightClick(tile: Tile, event: MouseEvent) {
    event.preventDefault();

    if (this.gameFinished) { return; }

    if (!tile.touched) {
      tile.hasFlag = !tile.hasFlag;
    }

    this.updateRemainingMines.emit(this.boardService.getNumberOfMines() - this.boardService.getNumberOfFlags());
  }

  tileOnClick(tile: Tile, event: MouseEvent) {
    if (tile.touched || tile.hasFlag || this.gameFinished) { return; }

    if (tile.isAMine) {
      tile.touched = true;
      this.gameFinished = true;
      this.gameLosed.emit();
      return;
    }
    this.boardService.searchForMines(tile);

    tile.touched = true;

    if (this.playerWins()) {
      this.gameFinished = true;
      this.gameWon.emit();
    }

  }

  playerWins() {
    return this.boardService.getRemainingTiles() === 0;
  }

}
