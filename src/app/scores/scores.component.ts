import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ScoresService } from './scores.service';
import { Match } from '../match/match';

@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.css'],
  providers: [ScoresService]
})
export class ScoresComponent implements OnInit {

  public matches: Array<Match>;

  constructor(private location: Location, private scoresService: ScoresService) { }

  ngOnInit() {
   this.matches = this.scoresService.getMatches();
  }

  goBack(): void {
    this.location.back();
  }

}
