import { Injectable } from '@angular/core';


@Injectable()
export class ScoresService {

  public getMatches() {
    const matches = JSON.parse(localStorage.getItem('matches'));
    return matches;
  }
}
