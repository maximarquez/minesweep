import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SetupComponent } from './setup/setup.component';
import { ScoresComponent } from './scores/scores.component';
import { MatchComponent } from './match/match.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'setup', component: SetupComponent },
  { path: 'match', component: MatchComponent },
  { path: 'scores', component: ScoresComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  exports: [ RouterModule ],
  declarations: [],
  imports: [ RouterModule.forRoot(appRoutes) ]
})
export class AppRoutingModule { }
