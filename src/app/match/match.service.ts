import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Match } from './match';


@Injectable()
export class MatchService {

  public saveMatch(match: Match) {
    let matches = JSON.parse(localStorage.getItem('matches'));
    if (!matches) {
      matches = new Array<Match>();
    }

    matches.push(match);

    localStorage.setItem('matches', JSON.stringify(matches));
  }
}
