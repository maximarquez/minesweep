import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatchService } from './match.service';
import { Match } from './match';
import { SetupService } from '../setup/setup.service';
import { Router } from '@angular/router';

declare var moment: any;

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  public match: Match;

  public remainingMines;

  public gameFinished: boolean;

  public gameResultMessage: string;

  constructor(private matchService: MatchService, private setupService: SetupService,
    private location: Location, private router: Router) { }

  ngOnInit() {

    const difficulty = this.setupService.getSelectedDifficulty();

    if (!difficulty) { this.router.navigateByUrl('home'); }
    this.gameResultMessage = '';
    this.match = new Match();
    this.match.startTime = moment();
    this.match.difficulty = difficulty.name;
    this.gameFinished = false;
  }


  goBack(): void {
    this.location.back();
  }

  goHome(): void {
    this.router.navigateByUrl('home');
  }


  onGameWon() {
    this.match.endTime = moment();
    const duration = moment.duration(this.match.endTime.diff(this.match.startTime));
    this.match.totalTime = moment.utc(duration.as('milliseconds')).format('HH:mm:ss');
    this.match.status = 'WON';
    this.matchService.saveMatch(this.match);

    this.gameFinished = true;
    this.gameResultMessage = 'Congratulations! You win!!';
  }

  onGameLosed() {
    this.match.endTime = moment();
    const duration = moment.duration(this.match.endTime.diff(this.match.startTime));
    this.match.totalTime = moment.utc(duration.as('milliseconds')).format('HH:mm:ss');
    this.match.status = 'LOST';
    this.matchService.saveMatch(this.match);

    this.gameFinished = true;
    this.gameResultMessage = 'You lose';
  }

  updateRemainingMines(event: number) {
    this.remainingMines = event;
  }

}
