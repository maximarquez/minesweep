export class Match {
  public startTime: any;
  public endTime: any;
  public difficulty: string;
  public totalTime: string;
  public status: string;
}
