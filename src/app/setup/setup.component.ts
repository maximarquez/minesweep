import { Difficulty } from './difficulty';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { SetupService } from './setup.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit {

  public formCustom: FormGroup;

  constructor(private location: Location, private router: Router,
    private setupService: SetupService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formCustom = this.formBuilder.group({
      rows: [1, [Validators.min(1), Validators.required, Validators.max(45)]],
      columns: [1, [Validators.min(1), Validators.required, Validators.max(45)]],
      mines: [1, [Validators.min(1), Validators.required, Validators.max(45)]]
    });
  }

  get columns(): AbstractControl { return this.formCustom.get('columns'); }

  get rows(): AbstractControl { return this.formCustom.get('rows'); }

  get mines(): AbstractControl { return this.formCustom.get('mines'); }

  goBack(): void {
    this.location.back();
  }

  difficultySelected(elementIndex: number) {
    this.setupService.selectDifficulty(this.setupService.getPredefinedDifficultyByIndex(elementIndex));
    this.router.navigate(['match']);
  }

  setCustomDifficulty() {
    if (!this.formCustom.valid) { return; }

    const difficulty = new Difficulty();
    difficulty.name = 'Custom';
    difficulty.rows = this.rows.value;
    difficulty.columns = this.columns.value;
    difficulty.mines = this.mines.value;

    this.setupService.selectDifficulty(difficulty);

    this.router.navigate(['match']);
  }

}
