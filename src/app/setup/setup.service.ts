import { Difficulty } from './difficulty';
import { Injectable } from "@angular/core";

@Injectable()
export class SetupService {

  private selectedDifficulty: Difficulty;

  private difficulties: Array<Difficulty> = [{
    name: 'Easy', rows: 8, columns: 8, mines: 10
  }, { name: 'Medium', rows: 16, columns: 16, mines: 40 }
    , { name: 'Hard', rows: 24, columns: 24, mines: 99 }];

  public getPredefinedDifficulties(): Array<Difficulty> {
    return this.difficulties;
  }

  public getPredefinedDifficultyByIndex(index: number): Difficulty {
    return this.difficulties[index];
  }

  public selectDifficulty(difficulty: Difficulty) {
    this.selectedDifficulty = difficulty;
  }

  public getSelectedDifficulty(): Difficulty {
    return this.selectedDifficulty;
  }

}
