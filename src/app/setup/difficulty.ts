export class Difficulty {
  public name: string;
  public rows: number;
  public columns: number;
  public mines: number;
}
