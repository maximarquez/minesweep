import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SetupComponent } from './setup/setup.component';
import { ScoresComponent } from './scores/scores.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './/app-routing.module';

import { BoardComponent } from './board/board.component';
import { MatchComponent } from './match/match.component';

import { BoardService } from './board/board.service';
import { MatchService } from './match/match.service';
import { SetupService } from './setup/setup.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SetupComponent,
    ScoresComponent,
    BoardComponent,
    MatchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [BoardService, MatchService, SetupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
